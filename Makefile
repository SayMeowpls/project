client : Client.o
	gcc -Wall -o Client Client.o -std=c99 -lpthread
server : Server.o
	gcc -Wall -o Server Server.o -std=gnu99 -lpthread
client2 : Client2.o
	gcc -Wall -o Client2 Client2.o -std=gnu99 -lpthread
Server.o : Server.c 
	gcc -c Server.c -std=gnu99 -lpthread
Client.o : Client.c 
	gcc -c Client.c -std=c99 -lpthread
Client2.o : Client2.c 
	gcc -c Client2.c -std=gnu99 -lpthread

clean : 
	rm Client.o Client Server Server.o Client2.o Client2
