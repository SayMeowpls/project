#include <sys/ipc.h>
#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket() and bind() */
#include <arpa/inet.h>  /* for sockaddr_in */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <pthread.h>

#define MAXPENDING 5
#define TCPPORT2 "37000"
#define RCVBUFSIZE 64
#define MAXRECVSTRING 255
#define PORTRANGE 10  // Диапозон портов

void DieWithError(char *errorMessage)
{
    perror(errorMessage);
    exit(1);
}

int main(int argc, char *argv[])
{
    int length, sleep_time;
    srand(getpid());
    int sock, socktcp, strlength,recvStringLen, bytesRcvd, totalBytesRcvd;
    struct sockaddr_in broadcastAddr, echoServAddr;
    unsigned short broadcastPort, TCPport;
    char recvString[MAXRECVSTRING+1], echoBuffer[RCVBUFSIZE];
    char *finishstring;
    char *servIP;
    if (argc != 3){
        fprintf(stderr, "Usage:  %s <Server Port> (В диапазоне от 39000 до 39010, <Server IP>\n", argv[0]);
        exit(1);
    }


    broadcastPort = atoi(argv[1]);
    servIP = argv[2];
    TCPport = atoi(TCPPORT2);             

    memset(&broadcastAddr, 0, sizeof(broadcastAddr));
    broadcastAddr.sin_family = AF_INET;
    broadcastAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    broadcastAddr.sin_port  = htons(broadcastPort);

    memset(&echoServAddr, 0, sizeof(echoServAddr));
    echoServAddr.sin_family      = AF_INET;
    echoServAddr.sin_addr.s_addr = inet_addr(servIP);
    echoServAddr.sin_port        = htons(TCPport);

    for (;;){
        if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
            DieWithError("socket() failed");

        bind(sock, (struct sockaddr *) &broadcastAddr, sizeof(broadcastAddr));

        if ((recvStringLen = recvfrom(sock, recvString, MAXRECVSTRING, 0, NULL, 0)) < 0)
            DieWithError("recvfrom() failed");
        recvString[recvStringLen] = '\0';
        printf("Сообщение от сервера: %s\n", recvString);
        
        close(sock);

        if ((socktcp = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
            DieWithError("socket() failed");
        for (int i = 0; i < PORTRANGE + 1; i++){
            echoServAddr.sin_port = htons(TCPport + i);
            if (connect(socktcp, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) == 0){
                printf("i = %d\n", i);
                i = PORTRANGE + 1;
            }
        }

        bytesRcvd = recv(socktcp, (char *)&echoBuffer[0], RCVBUFSIZE, 0);
        close(socktcp);
        echoBuffer[bytesRcvd] = '\0';

        printf("Получено сообщение: время обработки %d секунд, размер сообщения %d байт\n", ((int *)echoBuffer)[0],((int *)echoBuffer)[1]);
        for(int i = sizeof(int) * 2; i < ((int*)echoBuffer)[1]-1; i++){
            printf("%c", echoBuffer[i]);
        }
        printf("\n");
        sleep(((int *)echoBuffer)[0]);
        close(socktcp);
    }
    exit(0);

}