#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <sys/types.h>

#define MAXRECVSTRING 255
#define RCVBUFSIZE 32
#define TCPPORT1 "35000"
#define MINSTRSIZE 10
    
void DieWithError(char *errorMessage)
{
    perror(errorMessage);
    exit(1);
}

// Функция генерации строки
char *randstringfunc(size_t length) {
    static char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";        
    char *randomString = NULL;
    if (length) {
        randomString = malloc(sizeof(char) * length);
        ((int*)randomString)[0] = rand()%10 + MINSTRSIZE;
        ((int*)randomString)[1] = length;
        if (randomString) {            
            for (int n = sizeof(int) * 2; n < length; n++) {            
                int key = rand() % (int)(sizeof(charset) -1);
                randomString[n] = charset[key];
            }
            randomString[length-1] = '\0';
        }
    }
    return randomString;
}

int main(int argc, char *argv[])
{
    int length, sleep_time;
    srand(getpid());
    int sock, socktcp, strlength,recvStringLen, bytesRcvd, totalBytesRcvd;
    struct sockaddr_in broadcastAddr, echoServAddr;
    unsigned short broadcastPort, echoServPort;
    char recvString[MAXRECVSTRING+1], echoBuffer[RCVBUFSIZE];
    char *finishstring;
    char *servIP;
    char *randomstring;
    unsigned int randomstringLen;
    if (argc != 3){
        fprintf(stderr, "Usage:  %s <Server Port> (В диапазоне от 32000 до 32010, <Server IP>\n", argv[0]);
        exit(1);
    }

    broadcastPort = atoi(argv[1]);
    servIP = argv[2];             
    echoServPort = atoi(TCPPORT1);

    memset(&broadcastAddr, 0, sizeof(broadcastAddr));
    broadcastAddr.sin_family = AF_INET;
    broadcastAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    broadcastAddr.sin_port  = htons(broadcastPort);

    memset(&echoServAddr, 0, sizeof(echoServAddr));
    echoServAddr.sin_family      = AF_INET;
    echoServAddr.sin_addr.s_addr = inet_addr(servIP);
    echoServAddr.sin_port        = htons(echoServPort);



    for (;;){
        if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
            DieWithError("socket() failed");

        if (bind(sock, (struct sockaddr *) &broadcastAddr, sizeof(broadcastAddr)) < 0)
            DieWithError("bind() failed");

        if ((recvStringLen = recvfrom(sock, recvString, MAXRECVSTRING, 0, NULL, 0)) < 0)
            DieWithError("recvfrom() failed");

        recvString[recvStringLen] = '\0';
        printf("Сообщение от сервера: %s\n", recvString);
        
        close(sock);

        if ((socktcp = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
            DieWithError("socket() failed");

        connect(socktcp, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr));

        randomstring = randstringfunc(MINSTRSIZE+rand()%10);
        randomstringLen = ((int *)randomstring)[1];

        if (send(socktcp, randomstring, randomstringLen, 0) != randomstringLen)
            DieWithError("send() sent a different number of bytes than expected");

        printf("Клиент отправил сообщение %d байт, Клиент ждет %d секунд:\n", ((int*)randomstring)[1],((int *)randomstring)[0]);
        for(int i = sizeof(int) * 2; i<((int*)randomstring)[1]-1; i++){
            printf("%c", randomstring[i]);
        }
        printf("\n"); 
        sleep(((int *)randomstring)[0]);
        close(socktcp);
    }
    // close(socktcp);
    // exit(0);
}