#include <sys/ipc.h>
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <strings.h>

#define PORT 32000
#define PORT2 37000
#define PORT3 39000
#define MAXPENDING 5
#define RCVBUFSIZE 64
#define TCPPORT1 "35000" // Порт для получения TCP сообщений
#define TCPPORT2 "37000"
#define MAXBUFFSIZE 5 // Максимальный размер очереди
#define SLEEPTIME 5 // Время проверки на наличие сообщений
#define PORTRANGE 10 // Диапозон портов
#define SLEEPINTERVAL 1 // интервал между бродкастом и бродкастом на следующий порт

struct node
{
    char msg[RCVBUFSIZE];
    struct node *next;
} *head, *var, *trav;

int bufsize = 0;

int insert_at_begning(char message[RCVBUFSIZE])
{
    if (bufsize == MAXBUFFSIZE){
        printf("Очередь заполнена, сообщение отброшено\n");
        return 0;
    }
    var = (struct node *)malloc(sizeof (struct node));
    bcopy(message, var->msg, ((int *)message)[1]);
    if(head == NULL)
    {
        head = var;
        head->next = NULL;
        printf("Сообщение помещено в очередь\n");
        bufsize += 1;
    }
    else
    {
        var->next=head;
        head = var;
        printf("Сообщение помещено в очередь\n");
        bufsize += 1;
    }
}

int delete_from_end() {
    struct node *temp;
    temp = head;
    while(temp->next != NULL) {
        var = temp;
        temp = temp->next;
    }
    if(temp == head) {
        head = NULL; 
        free(temp);
        bufsize -= 1;
        printf("Сообщение удалено из очереди, в очереди %d сообщений\n", bufsize);
        return 0;
    }
    var->next = NULL;
    free(temp);
    bufsize -= 1;
    printf("Сообщение удалено из очереди, в очереди %d сообщений\n", bufsize);
    return 0;
}

void DieWithError(char *errorMessage)
{
    perror(errorMessage);
    exit(1);
}

void *thread_tcp_rcv_func(void *arg){
    int servSock, clntSock, bytesRcvd;                
    struct sockaddr_in echoServAddr;
    struct sockaddr_in echoClntAddr;
    unsigned short echoServPort;
    unsigned int clntLen;
    char echoBuffer[RCVBUFSIZE];

    echoServPort = atoi(TCPPORT1);

    if ((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        DieWithError("socket() failed");
      
    memset(&echoServAddr, 0, sizeof(echoServAddr));
    echoServAddr.sin_family = AF_INET;
    echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    echoServAddr.sin_port = htons(echoServPort);

    if (bind(servSock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0)
        DieWithError("bind(1) failed");

    if (listen(servSock, MAXPENDING) < 0)
        DieWithError("listen() failed");
    for (;;) {
        clntLen = sizeof(echoClntAddr);
        if ((clntSock = accept(servSock, (struct sockaddr *) &echoClntAddr, &clntLen)) < 0)
            DieWithError("accept() failed");

        if ((bytesRcvd = recv(clntSock, (char *)&echoBuffer[0], RCVBUFSIZE, 0)) <= 0)
            DieWithError("recv() failed or connection closed prematurely");

        echoBuffer[bytesRcvd] = '\0';
        printf("Получено сообщение: время обработки %d секунд, размер сообщения %d байт\n", ((int *)echoBuffer)[0],((int *)echoBuffer)[1]);
        for(int i = sizeof(int) * 2; i < ((int*)echoBuffer)[1]-1; i++){
            printf("%c", echoBuffer[i]);
        }
        printf("\n");
        insert_at_begning(echoBuffer);
    }
    close(clntSock);
    close(servSock);
}

void *tcpsendfunc(void *arg){
    char msg[RCVBUFSIZE];
    int socktcp2, clntSock;
    struct sockaddr_in ServerAddr;
    struct sockaddr_in echoClntAddr;
    unsigned short ServPort;
    unsigned int clntLen;
             
    ServPort = atoi(TCPPORT2);

    memset(&ServerAddr, 0, sizeof(ServerAddr));
    ServerAddr.sin_family      = AF_INET;
    ServerAddr.sin_addr.s_addr = INADDR_ANY;
    ServerAddr.sin_port        = htons(ServPort);

    int i = 0;

    for(;;){
        if ((socktcp2 = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
            DieWithError("socket() failed");

        if (bind(socktcp2, (struct sockaddr *) &ServerAddr, sizeof(ServerAddr)) < 0)
            DieWithError("bind() failed");

        if (listen(socktcp2, MAXPENDING) < 0)
            DieWithError("listen() failed");

        clntLen = sizeof(echoClntAddr);

        if (head != NULL) {
            struct node *temp;
            temp = head;
            while(temp->next != NULL) {
                var = temp;
                temp = temp->next;
            }
            if(temp == head) {
                printf("Читаем сообщение из очереди\n");
                bcopy(head->msg, msg, ((int*)(head->msg))[1]);
                printf("Обработка займет %d секунд \n",((int *)msg)[0]);

                if ((clntSock = accept(socktcp2, (struct sockaddr *) &echoClntAddr, &clntLen)) < 0)
                    DieWithError("accept() failed");

                if (send(clntSock, (char *)msg, ((int *)msg)[1], 0) != ((int *)msg)[1])
                    DieWithError("send() sent a different number of bytes than expected");
                close(clntSock);
            }
            else {
                printf("Читаем сообщение из очереди\n");
                bcopy(temp->msg, msg, ((int*)(temp->msg))[1]);
                printf("Обработка займет %d секунд \n",((int *)msg)[0]);

                if ((clntSock = accept(socktcp2, (struct sockaddr *) &echoClntAddr, &clntLen)) < 0)
                    DieWithError("accept() failed");

                if (send(clntSock, (char *)msg, ((int *)msg)[1], 0) != ((int *)msg)[1])
                    DieWithError("send() sent a different number of bytes than expected");
                close(clntSock);
            }
            printf("i = %d\n",i);
            if (i < PORTRANGE)
                i += 1;
            else
                i = 0;
            ServerAddr.sin_port = htons(ServPort + i);            
            delete_from_end();
            close(socktcp2);
        }
        else {
            printf("Очередь пуста\n");
            sleep(SLEEPTIME);
        }
        close(socktcp2);
        close(clntSock);
    }
}

void *broadcastfunc(void *arg){
    int sock;
    struct sockaddr_in broadcastAddr;
    char *broadcastIP;
    unsigned short broadcastPort;
    char *sendString = "Сервер доступен для сообщений";
    int broadcastPermission;
    unsigned int sendStringLen;

    broadcastIP = arg;
    broadcastPort = PORT;

    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        DieWithError("socket() failed");

    broadcastPermission = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission,
            sizeof (broadcastPermission)) < 0)
        DieWithError("setsockopt() failed");

    memset(&broadcastAddr, 0, sizeof (broadcastAddr));
    broadcastAddr.sin_family = AF_INET;
    broadcastAddr.sin_addr.s_addr = inet_addr(broadcastIP);
    broadcastAddr.sin_port = htons(broadcastPort);

    sendStringLen = strlen(sendString);

    for (;;) {
        for (int i = 0 ; i < PORTRANGE; i++) {
            if (bufsize < MAXBUFFSIZE){
                if (sendto(sock, sendString, sendStringLen, 0, (struct sockaddr *)&broadcastAddr, sizeof (broadcastAddr)) != sendStringLen)
                    DieWithError("sendto() sent a different number of bytes than expected");
                sleep(SLEEPINTERVAL);
                broadcastAddr.sin_port = htons(broadcastPort+i);
            }
            else { 
                i--;
                sleep(SLEEPINTERVAL);
            }
        }
    }
}


void *broadcastforcln2(void *arg){
    int sock;
    struct sockaddr_in broadcastAddr;
    char *broadcastIP;
    unsigned short broadcastPort;
    char *sendString = "На сервере есть сообщения";
    int broadcastPermission;
    unsigned int sendStringLen;

    broadcastIP = arg;
    broadcastPort = PORT3;

    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        DieWithError("socket() failed");

    broadcastPermission = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission, sizeof (broadcastPermission)) < 0)
        DieWithError("setsockopt() failed");

    memset(&broadcastAddr, 0, sizeof (broadcastAddr));
    broadcastAddr.sin_family = AF_INET;
    broadcastAddr.sin_addr.s_addr = inet_addr(broadcastIP);
    broadcastAddr.sin_port = htons(broadcastPort);

    sendStringLen = strlen(sendString);

    for (;;) {
        for (int i = 0 ; i < PORTRANGE; i++) {
            if (bufsize > 0){
                if (sendto(sock, sendString, sendStringLen, 0, (struct sockaddr *)&broadcastAddr, sizeof (broadcastAddr)) != sendStringLen)
                    DieWithError("sendto() sent a different number of bytes than expected");

                sleep(SLEEPINTERVAL);
                broadcastAddr.sin_port = htons(broadcastPort+i);
            }
            else { 
                i--;
                sleep(SLEEPINTERVAL);
            }
        }
    }
}


int main(int argc, char *argv[]) {

    if (argc != 2){
        fprintf(stderr, "Usage:  %s <Broadcast IP>,\n", argv[0]);
        exit(1);
    }

    pthread_t threadbroadcast, threadtcprcv, threadsendtcp, broadcast2;

    pthread_create(&threadtcprcv, NULL, thread_tcp_rcv_func, NULL);
    pthread_create(&broadcast2, NULL, broadcastforcln2, argv[1]);
    pthread_create(&threadsendtcp, NULL, tcpsendfunc, NULL);
    pthread_create(&threadbroadcast, NULL, broadcastfunc, argv[1]);

    pthread_join(threadbroadcast, NULL);
    pthread_join(threadtcprcv, NULL);
    pthread_join(threadsendtcp, NULL);
    pthread_join(broadcast2, NULL);

}